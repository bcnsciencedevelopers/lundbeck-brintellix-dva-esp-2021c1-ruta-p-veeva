$(document).ready(function () {
    main();
    $("#menu-oculto li:nth-child(5)").addClass("active");
    $('.btn_prev').hide();
    $('.btn_next').on('click', function () {
        $('.btn_next').hide();
        $('.btn_prev').show();
        $('.content_pop').addClass('active');
    });
    $('.btn_prev').on('click', function () {
        $('.btn_prev').hide();
        $('.btn_next').show();
        $('.content_pop').removeClass('active');
    });

});