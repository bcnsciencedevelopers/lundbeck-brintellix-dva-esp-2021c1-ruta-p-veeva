/*
 * Init fastclick
 */
if ("addEventListener" in document) {
    document.addEventListener(
      "DOMContentLoaded",
      function () {
        FastClick.attach(document.body);
      },
      false
    );
  }
  
  //permite cambiar de slide
  function main() {
    
    $("#menu-oculto").append(`
    <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_001" >
        <div class="icon icon-cerebro"></div>
        <p>TDM</p>
    </li>
    <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_005" >
        <div class="icon icon-hoja"></div>
        <p>Sintomas<br> cognitivos<br> en TDM</p>
    </li>
    <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_007" >
        <div class="icon icon-btx"></div>
        <p>Brintellix<sup>®</sup> en<br>
        los síntomas<br>
        cognitivos</p>
    </li>
  <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_010" >
      <div class="icon icon-diana"></div>
      <p>Eficacia</p>
  </li>
  <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_012" >
      <div class="icon icon-farmaco"></div>
      <p>Dosis</p>
  </li>
  <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_017" >
      <div class="icon icon-escudo"></div>
      <p>Tolerabilidad</p>
  </li>
  <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_018" >
      <div class="icon icon-engranaje"></div>
      <p>Mecanismo<br /> de acción</p>
  </li>
  <li data-slide="BR_DVA_master_ESP_rutaP_2021C1_019" >
      <div class="icon icon-medalla"></div>
      <p>Mensajes<br />clave</p>
  </li>`);
  
    events();
  }
  
  function events() {
    $("[data-popup]").on("click", function () {
      $(".popup").removeClass("active");
      var pop = $(this).data("popup");
      if (pop !== "") {
        $("#" + pop).addClass("active");
      }
    });
  
    $("[data-ft]").on("click", function () {
      com.veeva.clm.gotoSlide("ES_FT_brintellix_000.zip", "ES_FT_brintellix");
    });
    
    $('[data-slide]').on('click', function () {
      var auxSlide = $(this).data('slide');
      if (window.navigator.standalone === undefined) {
          location.href = "../" + auxSlide + "/"+auxSlide+".html";
      } else {
          var attr = $(this).attr('data-presentation');
          if (typeof attr !== 'undefined' && attr !== false) {
              com.veeva.clm.gotoSlide(auxSlide + ".zip", auxPresentation);
          }
          else{
              com.veeva.clm.gotoSlide(auxSlide + ".zip");
          }
      }
  });
  
    //El Menú está ocultado , si se hace "touch" en el botón se despliega el menú principal
    $(".menu-home").on("click", function () {
      if ($(this).hasClass("active")) {
        $("#menu-oculto").css("right", "-15%");
  
        $(this).removeClass("active");
      } else {
        $(this).addClass("active");
        $("#menu-oculto").css("right", "0%");
      }
    });
    $("#button-tog").on("click", function () {
      $("#global").toggleClass("global-t");
      $("#button-tog").toggleClass("active");
    });
    $(".btn-notes").on("click", function () {
      $(this).addClass("active");
    });
    $(".btn-ref").on("click", function () {
      $(this).addClass("active");
    });
  
    $(".popup_close").on("click", function () {
      $(".btn-notes").removeClass("active");
      $(".btn-ref").removeClass("active");
    });
  }
  
  if (window.navigator.standalone === undefined) {
    localDevelopment();
  }
  
  function localDevelopment() {
    console.info("Local development is ready");
  
    var next = "";
    var prev = "";
    var aux_slide = window.location.pathname;
    var aux_slide_num = "";
    var aux_slide_cant = 0;
  
    aux_slide = aux_slide.replace("/index.html", "");
    aux_slide = aux_slide.replace(/^\/.*\//gim, "");
    aux_slide_num = aux_slide.replace(/^.*_/gim, "");
  
    aux_slide_num = aux_slide_num + "";
    aux_slide = aux_slide.replace(aux_slide_num, "");
    aux_slide_cant = aux_slide_num.length;
  
    // fixed
    aux_slide = "BR_DVA_master_ESP_rutaP_2021C1_"
    console.log(aux_slide_num);
    aux_slide_num = parseInt(aux_slide_num);
  
    //prev
    if (aux_slide_num > 0) {
      prev = aux_slide + zfill(aux_slide_num - 1, aux_slide_cant);
    } else {
      prev = aux_slide + zfill(aux_slide_num, aux_slide_cant);
    }
    next = aux_slide + zfill(aux_slide_num + 1, aux_slide_cant);
    console.log("prev: " + prev);
    console.log("next: " + next);
  
    var page = document.body;
    var mc = new Hammer(page);
    mc.get("swipe").set({ direction: Hammer.DIRECTION_ALL });
    mc.on("swipeup swipedown swipeleft swiperight", function (ev) {
      if (ev.type == "swipeleft") {
        //next
        if (next != "") {
          location.href = "../" + next + "/index.html";
        }
      } else if (ev.type == "swiperight") {
        //prev
        if (prev != "") {
          location.href = "../" + prev + "/index.html";
        }
      }
    });
  }
  
  function zfill(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */
  
    if (width <= length) {
      if (number < 0) {
        return "-" + numberOutput.toString();
      } else {
        return numberOutput.toString();
      }
    } else {
      if (number < 0) {
        return "-" + zero.repeat(width - length) + numberOutput.toString();
      } else {
        return zero.repeat(width - length) + numberOutput.toString();
      }
    }
  }
  